#!/bin/bash
echo $((1 + $(cat env/version))) > env/version
PROJECT_NAME=$(cat env/pkgname)
VERSION=1.$(cat env/version)
LANG=C

cat > filesystem/DEBIAN/changelog <<EOF
$PROJECT_NAME ($VERSION) unstable; urgency=medium

  * Some change

 -- author <$GITLAB_USER_EMAIL>  $(date '+%a, %d %h %Y %H:%M:%S %z')
EOF

cat > filesystem/DEBIAN/control <<EOF
Package: $PROJECT_NAME
Version: $VERSION
Maintainer: author <s.russkikh@corp.mail.ru>
Architecture: all
Section: misc
Description: create empty bot
Depends: dpkg-dev
Priority: optional
EOF
