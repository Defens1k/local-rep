#!/bin/bash
PKGNAME=$(cat env/pkgname)
VERSION=1.$(cat env/version)
scripts/deb.sh
cd filesystem
dpkg-deb --build . ../$(echo $PKGNAME)_$(echo $VERSION)_all.deb
mv ../*.deb ../build/
cd ..
